FROM python:3.7-alpine

WORKDIR /app/

RUN pip install flask

RUN mkdir /app/static/
RUN mkdir /app/templates/

COPY /code/static/* /app/static/
COPY /code/templates/* /app/templates/
COPY /code/app.py /app/app.py

ENTRYPOINT ["python", "/app/app.py"]