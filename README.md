# Web App

```mermaid
graph TD;
  Sketch_Layout-->Front_End_Architecture;
  Sketch_Layout-->Back_End_Architecture;
  Back_End_Architecture-->Database_Architecture;
  Database_Architecture-->Cloud_Architecture;
  Front_End_Architecture-->Cloud_Architecture;
  Cloud_Architecture-->Pipeline;
  Pipeline-->Front_And_Back_Development;
```

<h1>Project Progress</h1>

-  [ ] Layout
    -  [x] Home
    -  [ ] Trilhas
    -  [ ] Scoreboard
-  [ ] Front end Architecture
-  [ ] Back end Architecture
-  [x] Cloud architecture
-  [x] Pipeline


<h1>Content</h1>

-  [ ] home
-  [ ] trilhas
    -  [ ] descrição
    -  [ ] vídeo
    -  [ ] imagem
- [ ] scoreboard