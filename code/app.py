from flask import Flask, session, render_template, request, redirect
import os
import mysql.connector
from create_db import check_db, get_latest_post_id

db = mysql.connector.connect(host="127.0.0.1", user="root", password="cmFwYWR1cmFzIGRhdGFiYXNlIHBhc3N3b3Jk")
app = Flask(__name__)

cursor = db.cursor()

app.config["SECRET_KEY"] = os.urandom(24)
UPLOAD_FOLDER = '/static'
ALLOWED_EXTENSIONS = set(['png', 'jpg'])

check_db(cursor)

def get_session():
    if "user" in session:
        return session["user"]
    else:
        return False

@app.route("/")
def index():
    user = get_session()
    return render_template("index.html", user=user)

@app.route("/authentification_hidden_area")
def auth_page():
    user = get_session()
    return render_template("auth.html", user=user)

@app.route("/hidden_auth_form", methods=["POST", "GET"])
def auth_form():
    print(session)
    user = get_session()
    if request.form.get("password") == "dGhpc2lzdGhlcmFwYWR1cmFzYWRtaW5wYXNzd29yZA==":
        session["user"] = "admin"
        session["user_id"] = 1
        return redirect("/")
    else:
        return render_template("auth.html", error="Wrong password, pal", user=user)

@app.route("/post")
def post_page():
    user = get_session()
    try:
        if (session["user"] == "admin") & (session["user_id"] == 1):
            return render_template("post.html", user=session["user"])
        else:
            return redirect("/denied")
    except:
        return redirect("/denied")

@app.route("/upload", methods=["POST"])
def post():
    user = get_session()
    try:
        if (session["user"] == "admin") & (session["user_id"] == 1):
            banner = request.files.get("banner")
            card = request.files.get("card")
            images = request.files.getlist("images")
            title = request.form.get("title")
            text_content = request.form.get("text_content")
            
            print(banner)
            print(card)
            print(images)
            print(title)
            print(text_content)

            latest_post = get_latest_post_id(cursor)
            new_post = int(latest_post)+1

            print("latest", latest_post, "new", new_post)

            banner.save("%s_banner.png"%str(new_post))
            card.save("%s_card.png"%str(new_post))
            for i in range(0, len(images)):
                images[i].save("%s_image_%s.png"%(str(new_post), i))

            return redirect("/")
        else:
            return redirect("/denied")
    except:
        return redirect("/denied")


@app.route("/test")
def test():
    user = get_session()
    return render_template("test.html", user=user)


@app.route("/denied")
def deny():
    user = get_session()
    return render_template("denied.html", user=user)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port="80")