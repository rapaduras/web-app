import mysql.connector

db = mysql.connector.connect(host="127.0.0.1", user="root", password="cmFwYWR1cmFzIGRhdGFiYXNlIHBhc3N3b3Jk", port="3306", auth_plugin='mysql_native_password')

cursor = db.cursor()

def create_db(cursor):
    cursor.execute("CREATE DATABASE content")
    cursor.execute("USE content")
    cursor.execute("CREATE TABLE posts (id INT AUTO_INCREMENT, title VARCHAR(35) NOT NULL, text_content TEXT NOT NULL NULL, PRIMARY KEY (id))")
    db.commit()

def check_db(cursor):
    cursor.execute("SHOW DATABASES")
    databases = cursor.fetchall()
    if ("content",) in databases:
        print("success")
    else:
        print("database not found\nCreating[...]")
        create_db(cursor)
    cursor.execute("USE content")

def get_latest_post_id(cursor):
    cursor.execute("SELECT id FROM posts ORDER BY id DESC LIMIT 0, 1")
    latest_id = cursor.fetchall()[0][0]
    return latest_id

def get_all_posts(cursor):
    cursor.execute("SELECT * FROM posts")
    all = cursor.fetchall()
    return all

def new_post(cursor, title, text_content):
    cursor.execute("INSERT INTO posts (title, text_content) VALUES ('%s', '%s')"%(title, text_content))
    db.commit()


check_db(cursor)
# print(get_all_posts(cursor))
print(get_latest_post_id(cursor))